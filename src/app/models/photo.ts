    export class photo {
        data: string = null;
    }
    export class beforeAfter {
        before: photo = new photo();
        after: photo = new photo();
        description: string = null;

        constructor(description: string = null, beforeData: string = null, afterData: string = null) {
            this.description = description;
            this.before.data = beforeData;
            this.after.data = afterData;
        }
    }

    export class inspectionReport {
        address: string = null;
        datetime: string = null;
        clientName: string = null;
        beforeAfterList: Array<beforeAfter> = new Array<beforeAfter>();
        addBeforeAfter(description: string, beforeData: string = null, afterData: string = null) {
            this.beforeAfterList.push(new beforeAfter(description, beforeData, afterData));
        }
        clearBeforeAfter() {
            this.beforeAfterList = new Array<beforeAfter>();
        }
    }
