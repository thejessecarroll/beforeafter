import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validator } from '@angular/forms';
import { inspectionReport } from 'src/app/models/photo';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  @Input() report: inspectionReport = new inspectionReport();
  constructor() { }

  ngOnInit() {
  }

  addBeforeAfter(name: string = null) {
    if (name === null) {
      name = 'New Card';
    }
    this.report.addBeforeAfter(name);
  }
}
