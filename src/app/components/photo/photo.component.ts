import { Component, Input } from '@angular/core';
import { photo } from '../../models/photo';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent {
  ngOnInit() {
//    console.log('PhotoModel', this.model)
  }

  @Input('photo') model: photo = new photo();

  onFileDropped($event) {
    let fr = new FileReader();
    fr.onload = () => {
      (this.model.data as any) = fr.result;
      //console.log("Setting this.file", this.file)
    }
    fr.readAsDataURL($event.dataTransfer.files[0]);    //console.log($event);
  }
}
