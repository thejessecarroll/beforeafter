import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { beforeAfter } from '../../models/photo';

@Component({
  selector: 'app-before-after',
  templateUrl: './before-after.component.html',
  styleUrls: ['./before-after.component.scss']
})
export class BeforeAfterComponent implements OnInit {
  ngOnInit() {
//    console.log('BeforeAfterModel', this.model)
  }
/*
  before:PhotoComponent;
  after:PhotoComponent; = new PhotoComponent();
  */
  @Input('beforeAfter') model: beforeAfter = new beforeAfter();
  @Output('delete') deleteEmitter: EventEmitter<this> = new EventEmitter<this>();
  delete() {
    this.deleteEmitter.emit(this);
  }

  onEditDescription() {
    (document.activeElement as any).select();
  }

  blur(event: any) {
    event.preventDefault();
    (document.activeElement as any).blur();
  }

  export() {
  /*
    console.log(this.after.getFile());
    return {
      "description": this.description,
      "before": this.before.getFile(),
      "after": this.after.getFile()
    };
    */
  }
}
