import { Component, OnInit, Input } from '@angular/core';
import { inspectionReport } from 'src/app/models/photo';

@Component({
  selector: 'app-controlbar',
  templateUrl: './controlbar.component.html',
  styleUrls: ['./controlbar.component.scss']
})
export class ControlbarComponent implements OnInit {

  @Input() report: inspectionReport = new inspectionReport();
  constructor() { }

  ngOnInit() {
    this.report.addBeforeAfter('Card 1');
    this.report.addBeforeAfter('Card 2');
    this.report.addBeforeAfter('Card 3');
    this.report.addBeforeAfter('Card 4');
  }

  saveByteArray(data, name) {
    var a = document.createElement("a");
    document.body.appendChild(a);
    var blob = new Blob(data, {type: "text/json"}),
        url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = name;
    a.click();
    window.URL.revokeObjectURL(url);
  }

  save() {
    this.saveByteArray([(JSON.stringify(this.report) as any)], 'example.txt');
  }

  loadPicked($event: any) {
    if ($event.target.files.length > 0) {
      this.load($event.target.files[0]);
    }
  }

  loadDrop($event: any) {
    if ($event.dataTransfer.files.length > 0) {
      this.load($event.dataTransfer.files[0]);
    }
  }

  load(file: File) {
    let fr = new FileReader();
    fr.onload = () => {
      //(this.model as any) = JSON.parse([fr.result as string]);
      this.report.clearBeforeAfter();
      let modelLite = JSON.parse(atob((fr.result as any).split(',')[1]));
      this.report.clientName = modelLite.clientName;
      this.report.address = modelLite.address;
      this.report.datetime = modelLite.datetime;
      console.log(modelLite);
      for (let k in modelLite['beforeAfterList']) {
        let beforeAfter = modelLite['beforeAfterList'][k];
        let beforeData = beforeAfter.before.data || null;
        let afterData = beforeAfter.after.data || null;
        this.report.addBeforeAfter(beforeAfter.description, beforeData, afterData);
      }
    }
    fr.readAsDataURL(file);    //console.log($event);
  }
}
