import { Component, OnInit, Input } from '@angular/core';
import { beforeAfter } from '../../models/photo';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-before-after-list',
  templateUrl: './before-after-list.component.html',
  styleUrls: ['./before-after-list.component.scss'],

})
export class BeforeAfterListComponent implements OnInit {
  ngOnInit() {
//    console.log('PhotoBucketModel', this.model)
  }

  @Input('list') list: Array<beforeAfter> = new Array<beforeAfter>();
  constructor() {
  }

  dropBeforeAfter(event: CdkDragDrop<string[]>) { 
    if (event.previousContainer !== event.container) {
      return;
    }
    moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
  }

  deleteBeforeAfter(index: number) {
    this.list.splice(index, 1);
  }
}
