import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DndDirective } from './directives/dnd.directive';
import { BeforeAfterListComponent } from './components/before-after-list/before-after-list.component';
import { PhotoComponent } from './components/photo/photo.component';

import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatLabel } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BeforeAfterComponent } from './components/before-after/before-after.component';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ControlbarComponent } from './components/controlbar/controlbar.component';
import { MatDividerModule } from '@angular/material/divider';
import { ReportComponent } from './components/report/report.component';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    AppComponent,
    DndDirective,
    BeforeAfterListComponent,
    PhotoComponent,
    BeforeAfterComponent,
    ControlbarComponent,
    ReportComponent,
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
