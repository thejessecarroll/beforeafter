import { Component, OnInit } from "@angular/core";
import { inspectionReport } from "./models/photo";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  report: inspectionReport;
  ngOnInit() {
    this.report = new inspectionReport();
  }
}
