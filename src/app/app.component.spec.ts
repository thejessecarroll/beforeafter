import { DragDropModule } from '@angular/cdk/drag-drop';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AppComponent } from './app.component';
import { BeforeAfterListComponent } from './components/before-after-list/before-after-list.component';
import { BeforeAfterComponent } from './components/before-after/before-after.component';
import { ControlbarComponent } from './components/controlbar/controlbar.component';
import { PhotoComponent } from './components/photo/photo.component';
import { ReportComponent } from './components/report/report.component';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ReportComponent,
        ControlbarComponent,
        BeforeAfterListComponent,
        BeforeAfterComponent,
        PhotoComponent
      ],
      imports: [
        //BrowserModule,
        MatCardModule,
        MatButtonModule,
        // MatDividerModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        // BrowserAnimationsModule,
        FormsModule,
        DragDropModule
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
